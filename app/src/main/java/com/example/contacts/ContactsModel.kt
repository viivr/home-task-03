package com.example.contacts

data class ContactsModel(
    val id: String,
    var name: String,
    var surname: String,
    var phone: String,
    var email: String
    )