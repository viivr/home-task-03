package com.example.contacts

import android.app.Activity
import android.content.Intent
import android.content.Intent.parseUri
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Contacts.PeopleColumns.DISPLAY_NAME
import android.provider.ContactsContract
import android.provider.ContactsContract.DisplayNameSources.EMAIL
import android.provider.SimPhonebookContract.SimRecords.PHONE_NUMBER
import android.provider.Telephony.Mms.Part.TEXT
import android.util.Log
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.contacts.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    private val CONTACT_PERMISSION_CODE = 100
    private val CONTACT_PICK_CODE = 101
    private var contactLauncher: ActivityResultLauncher<Intent>? = null
    //var uri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        contactLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
                if (result.resultCode == RESULT_OK) {
                    //val data = result.data
                    val uri: Uri? = result.data?.data
                    Log.d("MyLog", "$uri")
                    //ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    val cursor = contentResolver.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        null,
                        null,
                        null
                    )
                    cursor?.let {
                        if (it.moveToFirst()) {
                            binding.tvName.text =
                                it.getString(it.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY))
                            binding.tvSurname.text =
                                it.getString(it.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME))
                            binding.tvPhoneNumber.text =
                                it.getString(it.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER))
                            binding.tvEmail.text =
                                it.getString(it.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Email.ADDRESS))
                        }
                    }
                    cursor?.close()
                }
            }

        binding.btnSelectContact.setOnClickListener {
            if (checkContactPermission()) {
                contactLauncher?.launch(
                    Intent(
                        Intent.ACTION_PICK,
                        ContactsContract.Contacts.CONTENT_URI
                    )
                )
            } else {
                requestContactPermission()
            }
        }
    }

    private fun checkContactPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            android.Manifest.permission.READ_CONTACTS
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestContactPermission() {
        val permission = arrayOf(android.Manifest.permission.READ_CONTACTS)
        ActivityCompat.requestPermissions(this, permission, CONTACT_PERMISSION_CODE)
    }
}